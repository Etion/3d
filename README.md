# Projet Dorica Castra

L'objectif du projet et d'utiliser le module alegoria de itowns pour animer le dorica castra de Mathieu Pernot. Les photographies doivent se présenter à l'endroit où elles ont été prises, et dans leurs positions relatives dans dorica castra.

### Utilisation

A l'aide d'un serveur http (`npm install -g http-server` par ex) lancer la page oriented_image.html : `http://localhost:8080/src/oriented_images.html`.

La page d'accueil donne accès à l'ensemble des photos, un clic sur une des photographies en haut à gauche transporte la caméra au lieu où elle a été prise. ensuite le bouton  dorica zoom en arrière et charge les autres images dans leurs positions relatives sur la caméra active.

### Captures d'écran

**image dans sa position initiale** :
![alt-text](original.png)

**images dans leurs positions relatives** :
![alt-text](dorica.png)
